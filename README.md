# Machine Learning for Data Analytics


## Introduction
• Supervised, semi-supervised and unsupervised learning

• Classification and Regression

• Machine Learning, Deep Learning and Reinforcement Learning

• CRISP-DM, KDD and SEMMA

## Supervised Learning
• Linear Regression

• Nearest Neighbour

• Gaussian Naive Bayes

• Decision Trees

• Support Vector Machine (SVM)

• Random Forest

## Unsupervised Learning
• Clustering

• Association

• Anomaly Detection

• Dimensionality Reduction

## Semi-Supervised Learning
• Natural Language Processing

## Case Studies
• Supervised Learning

• Unsupervised Learning

• Semi-Supervised Learning

• Reinforcement Learning

• Deep Learning

## Validation and Optimisation
• Validation (Re-substitution, Hold-out, K-fold cross-validation, LOOCV, Random subsampling,
Bootstrapping...)

• Optimisation (loss functions/cost functions, Gradient Descent, Momentum, AdaGrad, RMSProp,
Adam...)
